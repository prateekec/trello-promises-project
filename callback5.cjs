const boards=require('./boards.json');
const lists=require('./lists.json');
const cards=require('./cards.json');
const callback1=require('./callback1.cjs');
const callback2=require('./callback2.cjs');
const callback3=require('./callback3.cjs');

function callback4 (){
    setTimeout(() => {
        let id="";
        for(let idx in boards){
            if(boards[idx].name="Thanos"){
                id=boards[idx].id;
                break;
            }
        }
        callback1(id).then((data) =>{
            console.log(data);
            return callback2(data["id"]);
        }).then((data) =>{
            console.log(data);
            let id="";
            for(let idx in data){
                if (data[idx].name=="Mind" || data[idx].name=="Space"){
                    id=data[idx].id;
                    callback3(id).then(console.log)
                    .catch(console.log);
                }
            }
        }).catch(console.log);
    }, 3*1000);
}
module.exports=callback4;