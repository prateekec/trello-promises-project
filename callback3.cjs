let cards=require('./cards.json');

function callback3(id,cb){
    return new Promise((resolve,reject) => {
        setTimeout(() => {
            if(cards[id]!=undefined){
                resolve(cards[id]);
            }else{
                reject( new Error("not found id : "+id));
            }
        }, 3*1000);
    })
}
module.exports=callback3;