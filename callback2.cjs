let lists=require('./lists.json');

function callback2(id){
    return new Promise((resolve,reject) => {
        setTimeout(() => {
            if(lists[id]!=undefined){
                return resolve(lists[id]);
            }else{
                return reject( Error({name : "not found"}));
            }
        }, 3*1000);
    })
}

module.exports=callback2;