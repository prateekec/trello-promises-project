const boards=require('./boards.json');
function callback1(boardId){
    return new Promise ((resolve,reject) => {
        setTimeout(() => {
        for(let idx in boards){
            if(boards[idx].id==boardId){
                resolve(boards[idx]);
            }
        }
        reject( new Error({name : "not found"}));
        }, 3*1000);
    });
}

module.exports=callback1;